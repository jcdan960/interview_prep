package advanced

import (
	"fmt"
	"sync"
	"sync/atomic"
)

func AtomicityAndFutures(){
	testFutures()
	testAtomic()
}

func testAtomic(){
	var n int32
	var wg sync.WaitGroup
	const NBGOROUTINE = 15000000 // yes 15 millions goroutine

	for i:=0; i < NBGOROUTINE; i++{
		wg.Add(1)
		go func(){
			atomic.AddInt32(&n, 1)
			wg.Done()
		}()
	}
	wg.Wait()
	fmt.Println(atomic.LoadInt32(&n))

}

func testFutures(){
	first := 123

	output := add_five_future(first)

	added := <-output
	fmt.Println(added)
}


func add_five(a int) int{
	return a+5
}

func add_five_future(a int) chan int{
	future := make (chan int)
	go func() {
		future <- add_five(a)
	} ()

	return future
}
