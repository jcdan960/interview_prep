package advanced

import "fmt"

// example de typedef

type SuperFloatPtr *float64

// defining a interface class, with 1 method
type geometry interface {
	area() float64
	perim() float64
}

type rect struct{
	width, height float64
}

func (r rect) area() float64{
	return r.width * r.height
}

func (r rect) perim() float64{
	return 2*(r.width + r.height)
}

func (c circle) area() float64{
	return c.radius * c.radius
}

func (c circle) perim() float64{
	return 123
}

type circle struct {
	radius float64
}

func measure(g geometry){

	switch g.(type) {
	case circle:
		fmt.Println("Am a circle yeah")
	case rect:
		fmt.Println("Am a rectangle yeah")
	default:
		fmt.Println("not sure what I am")
	}
	fmt.Println(g)
	fmt.Println(g.area())
	fmt.Println(g.perim())
}


func Try_Interfaces(){
	cir := circle{12}
	rectangle := rect{9,4}

	measure(cir)
	measure(rectangle)


}

