package advanced

import (
	"fmt"
	"sync"
	"time"
)

//https://tour.golang.org/concurrency/9

type SafeCounter struct{
	mtx sync.Mutex
	v map[string]int
}

func (c* SafeCounter) Inc(key string){
	c.mtx.Lock()
	defer c.mtx.Unlock()
	c.v[key]++
}

func (c *SafeCounter) Value(key string) int {
	c.mtx.Lock()
	// Lock so only one goroutine at a time can access the map c.v.
	defer c.mtx.Unlock()
	return c.v[key]
}

func TestMutexes(){
	c := SafeCounter{v: make(map[string]int)}
	for i:=0 ; i < 1200; i++{
		go c.Inc("yo")
	}
	
	time.Sleep(time.Second)
	fmt.Println(c.Value("yo"))
}


func TestJoin(){
	var wg sync.WaitGroup
	for i :=0; i < 50; i++{
		// just a counter
		wg.Add(1)
		go CoolGoroutine(&wg)
	}
	wg.Wait()
	fmt.Println("All done")
}

func CoolGoroutine(wg *sync.WaitGroup){
	fmt.Println("yo")

	wg.Done()
}
