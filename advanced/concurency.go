package advanced

import (
	"fmt"
	"math/rand"
	"time"
)

func RunConcurency(){
	time.Sleep(2* time.Second)
	fmt.Println("\n\n\n\n\n\n Starting the concurency")

	channel := make(chan float32)


	go say("Hi!", channel)
	go say("Hello", channel)

	x,y := <-channel , <-channel

	fmt.Println("x is ", x, "and y is ", y)
}

func say(word string, c chan float32){
	for i:=1; i< 10; i++{
		fmt.Println(word)
		time.Sleep(200*time.Millisecond)
	}
	x := rand.Float32()
	// in the channel c, we enqueue x
	c <- x

	// close a channel, no more data going throu this
	if 12 > 13{
		close(c)
	}
}