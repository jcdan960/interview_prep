package advanced

import (
	"fmt"
	"math/rand"
	"runtime"
	"sync"
	"time"
)

// To wait for multiple goroutines to finish, we can use a wait group.
var mtx sync.Mutex

func Try_parallelism(){

	arr := make([]uint16, 2000)

	min := 102
	max := 30000

	for i ,_ := range arr{
		arr[i] = uint16(rand.Intn(max - min + 1) + min)
	}

	maxProcs := runtime.GOMAXPROCS(0)
	numCPU := runtime.NumCPU()

	fmt.Println("Value of GOMAXPROCS = ", maxProcs)
	fmt.Println("Value of numCPU = ", numCPU)

	go printout_numbers(arr)
	go printout_random()

	fmt.Println('y')

	RunConcurency()

}

func printout_random(){
	rand.Seed(time.Now().UnixNano())

	min := -10000
	max := -30

	for i :=0; i < 1500 ; i++{
		fmt.Println(rand.Intn(max - min + 1) + min)
	}
}

func printout_numbers(arr[]uint16){
	for _ ,val := range arr{
		fmt.Println(val)
	}
}


