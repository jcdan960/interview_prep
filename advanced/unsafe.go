package advanced

import (
	"fmt"
	"unsafe"
)

// Not type safe ptr
// https://go101.org/article/unsafe.html


func TestUnsafe(){
	var x uint32 = 123
	fmt.Println(unsafe.Sizeof(x))

	unsafePtr := unsafe.Pointer(&x)

	fmt.Println(x, unsafePtr)
}