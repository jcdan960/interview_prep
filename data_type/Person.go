package data_type

type Person struct {
	Age uint16
	Name string
}

// first parenthese , info is a method of the Person class
// second parentheses: info method takes no param in
// last parentheses: return a tuple of string, int

func (p Person) Info() (string, uint16) {
	return p.Name, p.Age
}


const MinAge = 18

// regular function
func IsAdult(p *Person) bool{
	return p.Age >= MinAge
}

// method
func (p *Person) IsVeryAdult() bool {
	return p.Age >= MinAge
}