package loops

import (
	"fmt"
	"math"
)

func MakeLoopGreatAgain(){
	//https://tour.golang.org/flowcontrol/2
	// regular for loop

	var sum = 0
	for i:=0 ; i< 39; i++{
		sum += i
	}

	fmt.Println(sum)

	sum2 := 1
	for ; sum2 < 1000; {
		sum2 += sum2
	}
	fmt.Println(sum2)


	//infinite loop
	for {
		break
	}
}

func pow(x, n, lim float64) float64 {
	if v := math.Pow(x, n); v < lim {
		return v
	} else {
		fmt.Printf("%g >= %g\n", v, lim)
	}
	// can't use v here, though
	return lim
}
