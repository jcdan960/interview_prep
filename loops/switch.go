package loops

import "fmt"

const (
	Stopped = iota
	Starting
	Running
	Stopping
	Unknown)

func TrySwitching(){

	var currentState = Stopped

	switch currentState{
	case Stopped:
		fmt.Println("We are stopped mates")
	case Starting:
		print("We are starting")
	default:
		print("not sure")
	}


	//https://tour.golang.org/flowcontrol/9

}
