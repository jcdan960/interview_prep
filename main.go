package main
/*
https://sudonull.com/post/26785-Go-data-structures-cheat-sheet

rune // alias for int32
Go's basic types are

bool

string

int  int8  int16  int32  int64
uint uint8 uint16 uint32 uint64 uintptr

byte // alias for uint8

rune // alias for int32
     // represents a Unicode code point

float32 float64

complex64 complex128
*/

import (
	"fmt"
	"interview_prep/advanced"
	"interview_prep/data_type"
	"interview_prep/loops"
	"reflect"
)


/*
x := 3   /// use this notation when the variable need init
var x    // when no init required
 */


/*
 Go always executes function called main in the package called main
 and once this function terminates, so does the whole program (including running routines).
 */

func test_slices_and_array(){

	arr := [3]float32{2.3,3.5,4.4}

	arrCopy := arr

	for i,s := range arrCopy{
		fmt.Println(i,s)
	}

	///////slices

	slice := []uint16{3,4,5,4,123,5,6}

	fmt.Println(slice[0])

	slice = append(slice, 0,1,32,21,113)

	fmt.Println("Let prints some slice")

	for _, s:= range slice{
		fmt.Println(s)
	}

}

func test_defering(){
	defer fmt.Println("world")
	fmt.Println("Hello")
}

func test_hashMap(){
	m := make(map[uint16]string)
	
	m[0] = "first id mate"
	m[12] = "not the first mate"
	
	fmt.Println(m[0])
	fmt.Println(len(m))

	key, val := m[0]

	fmt.Println(val, key)

	delete(m, 12)

	commits := map[string]int{
		"rsc": 3711,
		"r":   2138,
		"gri": 1908,
		"adg": 912,
	}

	for key, element := range commits {
		fmt.Println("Key:", key, "Value:", element)
	}
}

func test_casting(){

	var my_int uint8 = 3

	var my_int_ptr *uint8 = &my_int

	if my_int_ptr != nil{
		fmt.Println("not nill mofo")
	}

	var second_int uint16 = uint16(*my_int_ptr)

	fmt.Println(second_int)
}

func test_naked_return() (x int){
	x = 3
	return
	//will just return 3
}

func test_slices_and_array_as_param(my_arr [8]int, my_slice[]bool){
	fmt.Println("Array:")

	for index, value := range my_arr{
		fmt.Println(index, value)
	}

	fmt.Println("Slice:")

	for index, value := range my_slice{
		fmt.Println(index, value)
	}

	var s = my_slice[0:3] //

	for index, value := range s{
		fmt.Println(index, value)
	}

}

func main(){
	jc :=  data_type.Person{30, "JC"}

	fmt.Printf("%s is %d \n", jc.Name, jc.Age)

	if jc.IsVeryAdult(){
		fmt.Println("Yes he is an adult")
	}

	test_defering()
	fmt.Println(jc.Info())

	fmt.Println(test_naked_return())

	//multiple declaration:

	var c, python, java bool = true, true, false
	fmt.Println(c, java, python)
	// au lieu de var  utiliser :=
	test  := 14

	fmt.Println(test)

	fmt.Println(reflect.TypeOf(test))

	var super_arr = [8]int{5,6,78,12,3,5,7,8}
	var super_slice = []bool{true,false,false,true,false,false}

	fmt.Println(reflect.TypeOf(super_slice))

	test_slices_and_array_as_param(super_arr, super_slice)

	const ta_mere = 3

	loops.MakeLoopGreatAgain()

	loops.TrySwitching()


	test_casting()
	test_slices_and_array()
	test_hashMap()

	advanced.Try_Interfaces()
	advanced.Try_parallelism()
	advanced.TestMutexes()
	advanced.TestJoin()
	advanced.AtomicityAndFutures()
	//advanced.MakeManyRequests()
	advanced.TestUnsafe()

	return
}